// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "MonolithUniverseProjectile.generated.h"

UCLASS()
class MONOLITHUNIVERSE_API AMonolithUniverseProjectile : public AActor
{
	GENERATED_BODY()

	/** Sphere collision component */
	UPROPERTY(VisibleDefaultsOnly, Category = Projectile)
	class USphereComponent* collisionComp;

	/** Projectile movement component */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
		class UProjectileMovementComponent* projectileMovement;

public:
	// Sets default values for this actor's properties
	AMonolithUniverseProjectile();

	void AssignTeam(int teamNumber);
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	int sourceTeam = -1;
public:
	/** called when projectile hits something */
	UFUNCTION()
	void OnHit(UPrimitiveComponent* hitComp, AActor* otherActor, UPrimitiveComponent* otherComp, FVector normalImpulse, const FHitResult& hit);
	UFUNCTION(BlueprintImplementableEvent)
	void OnDestroy(FVector spawnLocation);

	/** Returns CollisionComp subobject **/
	FORCEINLINE class USphereComponent* GetCollisionComp() const { return collisionComp; }
	/** Returns ProjectileMovement subobject **/
	FORCEINLINE class UProjectileMovementComponent* GetProjectileMovement() const { return projectileMovement; }
};
