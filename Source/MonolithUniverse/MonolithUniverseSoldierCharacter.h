// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "MonolithUniverseBaseCharacter.h"
#include "MonolithUniverseSoldierCharacter.generated.h"

/**
 * 
 */
UCLASS()
class MONOLITHUNIVERSE_API AMonolithUniverseSoldierCharacter : public AMonolithUniverseBaseCharacter
{
	GENERATED_BODY()
	
public:
	/** Cooldown for enhanced fire skill*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Skills)
	float superRunDurationTime = 2.0f;

	UFUNCTION(Server, Reliable, WithValidation)
	void OnEnhancedFire() override;
	UFUNCTION(Server, Reliable, WithValidation)
	void OnMovementSkillActivated() override;
	void OnMovementSkillEnds();
};
