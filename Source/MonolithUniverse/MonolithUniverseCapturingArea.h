// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "MonolithUniverseCapturingArea.generated.h"

UCLASS()
class MONOLITHUNIVERSE_API AMonolithUniverseCapturingArea : public AActor
{

	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AMonolithUniverseCapturingArea();

	class UBoxComponent* box;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Capturing")
	class UPointLightComponent* light;

	/** Time in seconds that one person need to stand in area to capture flag. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Capturing")
	float timeToCaptureArea;

	UFUNCTION(BlueprintImplementableEvent)
	void OnNeutralized();
	UFUNCTION(BlueprintImplementableEvent)
	void OnRedTeamCaptured();
	UFUNCTION(BlueprintImplementableEvent)
	void OnBlueTeamCaptured();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
		void Debug(FString message);
	UFUNCTION()
		void TriggerEnter(class UPrimitiveComponent* hitComp, class AActor* otherActor,
			class UPrimitiveComponent* otherComp, int32 otherBodyIndex, bool bFromSweep, const FHitResult & sweepResult);
	UFUNCTION()
		void TriggerExit(class UPrimitiveComponent* hitComp, class AActor* otherActor,
			class UPrimitiveComponent* otherComp, int32 otherBodyIndex);

	void UpdateCapturingPoints();
	void CapturingByTeam();
	void NeutralizingByTeam();
	void NeutralizeCapturingArea();
	int CapturingPower();
	bool ShouldUpdateCapturingPoints();
	UFUNCTION(NetMulticast, Reliable)
	void CapturedByBlueTeam();
	void AddBlueTeamPoints();
	UFUNCTION(NetMulticast, Reliable)
	void CapturedByRedTeam();
	void AddRedTeamPoints();
	UFUNCTION(NetMulticast, Reliable)
	void Neutralized();

private:
	/** Value between -100 and 100. Negative values for blue team, positive values for red team.*/
	UPROPERTY(ReplicatedUsing = OnRep_CapturingPoints)
	int capturingPoints;
	UFUNCTION()
	void OnRep_CapturingPoints();
	/** -1 if captured by blue, 0 if neutral, 1 if captured by red team */
	UPROPERTY(ReplicatedUsing = OnRep_CapturingState)
	int capturingState;
	UFUNCTION()
	void OnRep_CapturingState();

	int blueTeamPlayers;
	int redTeamPlayers;
	FTimerHandle addingPointsTimerHandle;

	UFUNCTION(NetMulticast, Reliable)
	void SetLightColor(FColor color);
};
