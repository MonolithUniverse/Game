// Fill out your copyright notice in the Description page of Project Settings.

#include "MonolithUniverse.h"
#include "MonolithUniverseBasicGameMode.h"
#include "MonolithUniverseBaseGameState.h"
#include "MonolithUniverseBaseCharacter.h"
#include "MonoltihUniverseBaseController.h"
#include "MonolithUniverseBaseHUD.h"
#include <fstream>

void AMonolithUniverseBasicGameMode::BeginPlay()
{
	Super::BeginPlay();
}

void AMonolithUniverseBasicGameMode::StartPlay()
{
	Super::StartPlay();

	for(TActorIterator<ASpawnPoint> it(GetWorld()); it; ++it)
	{
		if (it->GetTeamNumber() == 0)
			this->blueSpawnPoints.push_back(*it);
		else
			this->redSpawnPoints.push_back(*it);
	}
}

void AMonolithUniverseBasicGameMode::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);
	auto player = Cast<AMonolithUniverseBaseCharacter>(NewPlayer->GetPawn());

	if (player == NULL)
		return;

//	player->SetNickname("Nickname");

	OnPlayerJoined();
}

FString AMonolithUniverseBasicGameMode::InitNewPlayer(APlayerController * NewPlayerController, const FUniqueNetIdRepl & UniqueId, const FString & Options, const FString & Portal)
{
	auto controller = Cast<AMonoltihUniverseBaseController>(NewPlayerController);

	TArray<FString> Array;
	Options.ParseIntoArray(Array, *FString("?"));

	for (int i = 0; i < Array.Num(); i++) {
		TArray<FString> option;
		Array[i].ParseIntoArray(option, *FString("="));

		if (option[0].Compare("team") == 0) {
			controller->AssignTeam(FCString::Atoi(*option[1]));
		}
	}

	return Super::InitNewPlayer(NewPlayerController, UniqueId, Options, Portal);
}

FTransform AMonolithUniverseBasicGameMode::GetSpawnPointTransform(int teamNumber)
{
	FTransform result;

	if(teamNumber == 0)
	{
		result = blueSpawnPoints[currentBlueSpawn++]->GetTransform();
		
		if (currentBlueSpawn >= blueSpawnPoints.size())
			currentBlueSpawn = 0;
	}
	else
	{
		result = redSpawnPoints[currentRedSpawn++]->GetTransform();

		if (currentRedSpawn >= redSpawnPoints.size())
			currentRedSpawn = 0;
	}

	return result;
}

void AMonolithUniverseBasicGameMode::TeleporPlayerToSpawnPoint(AMonolithUniverseBaseCharacter* player)
{
	if (Role == ROLE_Authority)
	{
		FTransform transform = GetSpawnPointTransform(player->GetTeamNumber());
		player->SetActorTransform(transform);
	}
}

void AMonolithUniverseBasicGameMode::OnPlayerJoined()
{
	if (Role == ROLE_Authority)
	{
		if (PlayersAmount() == this->minPlayersAmount)
		{
			auto world = GetWorld();
			if (world)
			{
				auto gameState = Cast<AMonolithUniverseBaseGameState>(world->GetGameState());
				gameState->OnPrepareBattle();
			}
		}
	}
}


