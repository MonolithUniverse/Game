// Fill out your copyright notice in the Description page of Project Settings.

#include "MonolithUniverse.h"
#include "MonolithUniverseCapturingArea.h"
#include "MonolithUniverseBaseCharacter.h"
#include "MonolithUniverseBaseGameState.h"
#include "UnrealNetwork.h"
#include "MonolithUniverseBaseHUD.h"
#include "MonoltihUniverseBaseController.h"

// Sets default values
AMonolithUniverseCapturingArea::AMonolithUniverseCapturingArea()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	this->bReplicates = true;
	this->bAlwaysRelevant = true;

	this->capturingPoints = 0;
	this->capturingState = 0;
	this->blueTeamPlayers = 0;
	this->redTeamPlayers = 0;

	this->box = CreateDefaultSubobject<UBoxComponent>(TEXT("Box"));
	this->light = CreateDefaultSubobject<UPointLightComponent>(TEXT("Light"));

	this->box->SetupAttachment(RootComponent);
	this->box->bGenerateOverlapEvents = true;
	this->box->SetRelativeScale3D(FVector(10, 10, 2));
	this->box->OnComponentBeginOverlap.AddDynamic(this, &AMonolithUniverseCapturingArea::TriggerEnter);
	this->box->OnComponentEndOverlap.AddDynamic(this, &AMonolithUniverseCapturingArea::TriggerExit);

	this->light->SetupAttachment(RootComponent);
	this->light->SetIntensity(100000);
	this->light->SetLightColor(FColor::White);
	this->light->SetIsReplicated(true);
}

// Called when the game starts or when spawned
void AMonolithUniverseCapturingArea::BeginPlay()
{
	Super::BeginPlay();

	if (Role == ROLE_Authority)
	{
		FTimerHandle timerHandle;
		GetWorld()->GetTimerManager().SetTimer(timerHandle, this, &AMonolithUniverseCapturingArea::UpdateCapturingPoints,
			0.1f, true);
		OnNeutralized();
	}
}

// Called every frame
void AMonolithUniverseCapturingArea::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AMonolithUniverseCapturingArea::Debug(FString message)
{
//	if (GEngine)
//		GEngine->AddOnScreenDebugMessage(-1, 1, FColor::Red, message);
}

void AMonolithUniverseCapturingArea::TriggerEnter(UPrimitiveComponent * hitComp, AActor * otherActor, UPrimitiveComponent * otherComp, int32 otherBodyIndex, bool bFromSweep, const FHitResult & sweepResult)
{
	AMonolithUniverseBaseCharacter* character = Cast<AMonolithUniverseBaseCharacter>(otherActor);
	if (character != NULL)
	{
		if (character->GetTeamNumber() == 0)
			this->blueTeamPlayers++;
		else
			this->redTeamPlayers++;
	}
}

void AMonolithUniverseCapturingArea::TriggerExit(UPrimitiveComponent * hitComp, AActor * otherActor, UPrimitiveComponent * otherComp, int32 otherBodyIndex)
{
	AMonolithUniverseBaseCharacter* character = Cast<AMonolithUniverseBaseCharacter>(otherActor);
	if (character != NULL)
	{
		if (character->GetTeamNumber() == 0)
			this->blueTeamPlayers--;
		else
			this->redTeamPlayers--;
	}
}

void AMonolithUniverseCapturingArea::UpdateCapturingPoints()
{
	if (Role == ROLE_Authority)
	{
		if (ShouldUpdateCapturingPoints())
		{
			if (capturingState == 0) // capturing area is in neutral position
			{
				if (CapturingPower() == 0)
					NeutralizeCapturingArea();
				else
					CapturingByTeam();
			}
			else if (capturingState == -1) // area is captured by blue team
			{
				if (this->blueTeamPlayers > this->redTeamPlayers)
					CapturingByTeam();
				else
					NeutralizingByTeam();
			}
			else if (capturingState == 1) // area is captured by red team
			{
				if (this->redTeamPlayers > this->blueTeamPlayers)
					CapturingByTeam();
				else
					NeutralizingByTeam();
			}

			if (capturingPoints > 100) capturingPoints = 100;
			if (capturingPoints < -100) capturingPoints = -100;
//			Debug("CapturingPoints: " + FString::FromInt(this->capturingPoints));
		}
	}
}

void AMonolithUniverseCapturingArea::CapturingByTeam()
{
	if (Role == ROLE_Authority)
	{
		if (this->blueTeamPlayers > this->redTeamPlayers)
		{
//			Debug("CapturingByTeam(Blue) " + FString::FromInt(CapturingPower()));
			this->capturingPoints -= CapturingPower();
			if (this->capturingPoints <= -100)
				CapturedByBlueTeam();
		}
		else if (this->blueTeamPlayers < this->redTeamPlayers)
		{
//			Debug("CapturingByTeam(Red) " + FString::FromInt(CapturingPower()));
			this->capturingPoints += CapturingPower();
			if (this->capturingPoints >= 100)
				CapturedByRedTeam();
		}
	}
}

void AMonolithUniverseCapturingArea::NeutralizingByTeam()
{
	if (Role == ROLE_Authority)
	{
		if (this->blueTeamPlayers > this->redTeamPlayers)
		{
//			Debug("NeutralizingByTeam(Blue) " + FString::FromInt(CapturingPower()));
			this->capturingPoints -= CapturingPower();
			if (this->capturingPoints <= 0)
				Neutralized();
		}
		else if (this->blueTeamPlayers < this->redTeamPlayers)
		{
//			Debug("NeutralizingByTeam(Red) " + FString::FromInt(CapturingPower()));
			this->capturingPoints += CapturingPower();
			if (this->capturingPoints >= 0)
				Neutralized();
		}
	}
}

void AMonolithUniverseCapturingArea::NeutralizeCapturingArea()
{
	if (Role == ROLE_Authority)
	{
//		Debug("Neutralizing");
		if (this->capturingPoints > 0)
		{
			capturingPoints--;
		}
		else if (this->capturingPoints < 0)
		{
			capturingPoints++;
		}
	}
}

int AMonolithUniverseCapturingArea::CapturingPower()
{
	return FMath::Abs(this->blueTeamPlayers - this->redTeamPlayers);
}

bool AMonolithUniverseCapturingArea::ShouldUpdateCapturingPoints()
{
	if (capturingState == 0) // capturing area is in neutral position
	{
		return CapturingPower() != 0 || this->capturingPoints != 0;
	}
	else if (capturingState == -1) // area is captured by blue team
	{
		return this->redTeamPlayers > this->blueTeamPlayers 
			|| (this->blueTeamPlayers > this->redTeamPlayers && this->capturingPoints > -100);
	}
	else if (capturingState == 1) // area is captured by red team
	{
		return this->blueTeamPlayers > this->redTeamPlayers 
			|| (this->redTeamPlayers > this->blueTeamPlayers && this->capturingPoints < 100);
	}
	else
	{
		return false;
	}
}

void AMonolithUniverseCapturingArea::CapturedByBlueTeam_Implementation()
{
	// if already captured do nothing
	if (this->capturingState == -1)
		return;
	// change capturing state
	this->capturingState = -1;
	this->SetLightColor(FColor::Blue);
	OnBlueTeamCaptured();
	// start adding points for blue team in game state
	GetWorld()->GetTimerManager().SetTimer(this->addingPointsTimerHandle, this, &AMonolithUniverseCapturingArea::AddBlueTeamPoints,
		1.0f, true);
}

void AMonolithUniverseCapturingArea::AddBlueTeamPoints()
{
//	Debug("AddBlueTeamPoints");
	AMonolithUniverseBaseGameState* myGameState = 
		GetWorld() != NULL ? GetWorld()->GetGameState<AMonolithUniverseBaseGameState>() : NULL;
	if (myGameState != NULL)
		myGameState->AddBlueTeamPoints();
}

void AMonolithUniverseCapturingArea::CapturedByRedTeam_Implementation()
{
	// if already captured do nothing
	if (this->capturingState == 1)
		return;
	// change capturing state
	this->capturingState = 1;
	this->SetLightColor(FColor::Red);
	OnRedTeamCaptured();
	// start adding points for red team in game state
	GetWorld()->GetTimerManager().SetTimer(this->addingPointsTimerHandle, this, &AMonolithUniverseCapturingArea::AddRedTeamPoints,
		1.0f, true);
}

void AMonolithUniverseCapturingArea::AddRedTeamPoints()
{
//	Debug("AddRedTeamPoints");
	AMonolithUniverseBaseGameState* myGameState =
		GetWorld() != NULL ? GetWorld()->GetGameState<AMonolithUniverseBaseGameState>() : NULL;
	if (myGameState != NULL)
		myGameState->AddRedTeamPoints();
}

void AMonolithUniverseCapturingArea::Neutralized_Implementation()
{
	// change capturing state
	this->capturingState = 0;
	this->light->SetLightColor(FColor::White);
	OnNeutralized();
	// stop adding points for any team in game state
	GetWorldTimerManager().ClearTimer(this->addingPointsTimerHandle);
}

void AMonolithUniverseCapturingArea::OnRep_CapturingPoints()
{
	auto controller = Cast<AMonoltihUniverseBaseController>(GetWorld()->GetFirstPlayerController());
	auto HUD = Cast<UMonolithUniverseBaseHUD>(controller->GetHUDInstance());

	HUD->UpdateCapturingPoints(abs(this->capturingPoints));
}

void AMonolithUniverseCapturingArea::OnRep_CapturingState()
{
	auto controller = Cast<AMonoltihUniverseBaseController>(GetWorld()->GetFirstPlayerController());
	auto HUD = Cast<UMonolithUniverseBaseHUD>(controller->GetHUDInstance());

	HUD->UpdateCapturingState(this->capturingState);
}

void AMonolithUniverseCapturingArea::SetLightColor_Implementation(FColor color)
{
	this->light->SetLightColor(color);
}

void AMonolithUniverseCapturingArea::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(AMonolithUniverseCapturingArea, capturingState, COND_SimulatedOnly);
	DOREPLIFETIME_CONDITION(AMonolithUniverseCapturingArea, capturingPoints, COND_SimulatedOnly);
}