// Fill out your copyright notice in the Description page of Project Settings.

#include "MonolithUniverse.h"
#include "MonoltihUniverseBaseController.h"

void AMonoltihUniverseBaseController::BeginPlay()
{
	DisplayHUD();
}

void AMonoltihUniverseBaseController::DisplayHUD()
{
	if (Role < ROLE_Authority)
	{
		if (this->widgetInstance)
		{
			this->widgetInstance->RemoveFromViewport();
			this->widgetInstance = nullptr;
		}

		if (this->widgetTemplate)
		{
			this->widgetInstance = CreateWidget<UUserWidget>(this, this->widgetTemplate);
			this->widgetInstance->AddToViewport();
		}

	}
}

void AMonoltihUniverseBaseController::AssignTeam(int newTeamNumber)
{
	this->teamNumber = newTeamNumber;
}

int AMonoltihUniverseBaseController::GetTeamNumber()
{
	return this->teamNumber;
}

void AMonoltihUniverseBaseController::SetUsername(FString newUsername)
{
	this->username = newUsername;
}

FString AMonoltihUniverseBaseController::GetUsername()
{
	return this->username;
}


