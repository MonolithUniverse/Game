// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"
#include "MonolithUniverseBaseCharacter.h"
#include "MonoltihUniverseBaseController.generated.h"

/**
 * 
 */
UCLASS()
class MONOLITHUNIVERSE_API AMonoltihUniverseBaseController : public APlayerController
{
	GENERATED_BODY()
	
protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UI")
	TSubclassOf<class UUserWidget> widgetTemplate;

	UUserWidget* widgetInstance;

	UPROPERTY()
	int teamNumber;

	UPROPERTY()
		FString username;
private:
	void DisplayHUD();

public:
	UUserWidget* GetHUDInstance() const { return this->widgetInstance; }
	UFUNCTION()
		void AssignTeam(int newTeamNumber);
	UFUNCTION()
		int GetTeamNumber();

	UFUNCTION()
		void SetUsername(FString newUsername);
	UFUNCTION()
		FString GetUsername();
};
