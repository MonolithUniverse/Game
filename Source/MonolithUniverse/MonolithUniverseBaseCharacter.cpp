// Fill out your copyright notice in the Description page of Project Settings.

#include "MonolithUniverse.h"
#include "MonolithUniverseBaseCharacter.h"
#include "UnrealNetwork.h"
#include "MonolithUniverseBaseHUD.h"
#include "MonoltihUniverseBaseController.h"
#include "MonolithUniverseBasicGameMode.h"
#include <fstream>

// Sets default values
AMonolithUniverseBaseCharacter::AMonolithUniverseBaseCharacter()
{
	this->baseTurnRate = 45.f;
	this->baseLookUpRate = 45.f;

	// Default values
	this->maxAmmo = 10;
	this->reloadTime = 2.f;
	this->primaryAtackFireCost = 1;
	this->enhancedAtackFireCost = 2;
	this->maxHP = 100;
	this->enhancedFireCooldown = 10;
	this->movementSkillCooldown = 10;
	this->bIsEnhancedFireAvailable = true;
	this->bIsMovementSkillAvailable = true;
	this->nickname = "---";

	this->muzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("MuzzleLocation"));
	this->muzzleLocation->SetupAttachment(RootComponent);

	this->nicknameText = CreateDefaultSubobject<UTextRenderComponent>(TEXT("Nickname"));
	this->nicknameText->SetupAttachment(RootComponent);

 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

void AMonolithUniverseBaseCharacter::InitializeAttributes()
{
	if (Role == ROLE_Authority)
	{
		auto controller = Cast<AMonoltihUniverseBaseController>(this->Controller);
		this->teamNumber = controller->GetTeamNumber();

		this->currentAmmo = this->maxAmmo;
		this->currentHP = this->maxHP;

		FTimerHandle timerHandle;
		GetWorld()->GetTimerManager().SetTimer(timerHandle, this, &AMonolithUniverseBaseCharacter::DisplayTeamOnHUD, 0.2f, false);
		RestartPlayer();
	}
}

// Called when the game starts or when spawned
void AMonolithUniverseBaseCharacter::BeginPlay()
{
	Super::BeginPlay();
	FTimerHandle timerHandle;
	GetWorld()->GetTimerManager().SetTimer(timerHandle, this, &AMonolithUniverseBaseCharacter::InitializeAttributes, 0.5f, false);
}

// Called every frame
void AMonolithUniverseBaseCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AMonolithUniverseBaseCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	check(PlayerInputComponent);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &AMonolithUniverseBaseCharacter::OnFire);
	PlayerInputComponent->BindAction("EnhancedFire", IE_Pressed, this, &AMonolithUniverseBaseCharacter::OnEnhancedFire);
	PlayerInputComponent->BindAction("Reload", IE_Pressed, this, &AMonolithUniverseBaseCharacter::TryToReload);
	PlayerInputComponent->BindAction("MovementSkill", IE_Pressed, this, &AMonolithUniverseBaseCharacter::OnMovementSkillActivated);

	PlayerInputComponent->BindAxis("MoveForward", this, &AMonolithUniverseBaseCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AMonolithUniverseBaseCharacter::MoveRight);
	PlayerInputComponent->BindAction("Walk", IE_Pressed, this, &AMonolithUniverseBaseCharacter::Walk);
	PlayerInputComponent->BindAction("Walk", IE_Released, this, &AMonolithUniverseBaseCharacter::Run);

	PlayerInputComponent->BindAxis("Turn", this, &AMonolithUniverseBaseCharacter::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AMonolithUniverseBaseCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &AMonolithUniverseBaseCharacter::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AMonolithUniverseBaseCharacter::LookUpAtRate);

	// Only for tests
	PlayerInputComponent->BindAction("HitMe", IE_Pressed, this, &AMonolithUniverseBaseCharacter::HitMe);
	PlayerInputComponent->BindAction("HealMe", IE_Pressed, this, &AMonolithUniverseBaseCharacter::HealMe);
}

void AMonolithUniverseBaseCharacter::Jump()
{
	if (this->bIsDead) return;
	Super::Jump();
}

bool AMonolithUniverseBaseCharacter::OnFire_Validate()
{
	return true;
}

void AMonolithUniverseBaseCharacter::OnFire_Implementation()
{
	if (this->bIsDead) return;
	if (CanShoot(primaryAtackFireCost))
	{
		LoseAmmo(primaryAtackFireCost);
		SpawnProjectile();
	}
	else
	{
		TryToReload();
	}
}

void AMonolithUniverseBaseCharacter::MoveForward(float val)
{
	if (this->bIsDead) return;
	if (val != 0.0f)
	{
		if (this->bIsRunning)
			val *= 2;
		AddMovementInput(GetActorForwardVector(), val/2);
	}
}

void AMonolithUniverseBaseCharacter::MoveRight(float val)
{
	if (this->bIsDead) return;
	if (val != 0.0f)
	{
		if (this->bIsRunning)
			val *= 2;
		AddMovementInput(GetActorRightVector(), val/2);
	}
}

void AMonolithUniverseBaseCharacter::TurnAtRate(float rate)
{
	AddControllerYawInput(rate * this->baseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AMonolithUniverseBaseCharacter::LookUpAtRate(float rate)
{
	AddControllerPitchInput(rate * this->baseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AMonolithUniverseBaseCharacter::Walk()
{
	this->bIsRunning = false;
}

void AMonolithUniverseBaseCharacter::DisplayTeamOnHUD_Implementation()
{
	auto controller = Cast<AMonoltihUniverseBaseController>(this->Controller);
	auto hud = Cast<UMonolithUniverseBaseHUD>(controller->GetHUDInstance());
	if (hud)
		hud->SetTeam(this->teamNumber);
}

void AMonolithUniverseBaseCharacter::OnRep_SetCurrentAmmo()
{
	if(Role < ROLE_Authority && this->Controller)
	{
		auto controller = Cast<AMonoltihUniverseBaseController>(this->Controller);
		auto hud = Cast<UMonolithUniverseBaseHUD>(controller->GetHUDInstance());
		if (hud)
			hud->UpdateAmmo(this->currentAmmo, this->maxAmmo);
	}
}

void AMonolithUniverseBaseCharacter::OnRep_SetCurrentHP()
{
	if (Role < ROLE_Authority && this->Controller)
	{
		auto controller = Cast<AMonoltihUniverseBaseController>(this->Controller);
		auto hud = Cast<UMonolithUniverseBaseHUD>(controller->GetHUDInstance());
		if (hud)
			hud->UpdateHP(this->currentHP, this->maxHP);
	}
}

void AMonolithUniverseBaseCharacter::OnReload_Implementation()
{
	auto controller = Cast<AMonoltihUniverseBaseController>(this->Controller);
	auto hud = Cast<UMonolithUniverseBaseHUD>(controller->GetHUDInstance());
	if (hud)
		hud->StartReloading(this->reloadTime);
}

void AMonolithUniverseBaseCharacter::Run()
{
	this->bIsRunning = true;
}

bool AMonolithUniverseBaseCharacter::CanShoot(int ammoCost)
{
	return (this->currentAmmo - ammoCost >= 0) && !this->bIsReloading;
}

void AMonolithUniverseBaseCharacter::LoseAmmo(int amount) 
{
	this->currentAmmo -= amount;
}

bool AMonolithUniverseBaseCharacter::TryToReload_Validate()
{
	return true;
}

void AMonolithUniverseBaseCharacter::TryToReload_Implementation()
{
	if (this->bIsDead) return;
	if (!this->bIsReloading && this->currentAmmo < this->maxAmmo)
	{
		this->bIsReloading = true;
		GetWorld()->GetTimerManager().SetTimer(this->reloadTimer, this, &AMonolithUniverseBaseCharacter::Reload, this->reloadTime);
		OnReload();
	}
}
void AMonolithUniverseBaseCharacter::Reload()
{
	this->currentAmmo = this->maxAmmo;
	this->bIsReloading = false;
	GetWorld()->GetTimerManager().ClearTimer(this->reloadTimer);
}

void AMonolithUniverseBaseCharacter::SpawnProjectile()
{
	if (projectileClass != NULL)
	{
		UWorld* const World = GetWorld();
		if (World != NULL)
		{
			const FRotator spawnRotation = GetControlRotation();
			// MuzzleOffset is in camera space, so transform it to world space before offsetting from the character location to find the final muzzle position
			const FVector SpawnLocation = ((muzzleLocation != nullptr) ? muzzleLocation->GetComponentLocation() : GetActorLocation()) + spawnRotation.RotateVector(gunOffset);

			//Set Spawn Collision Handling Override
			FActorSpawnParameters ActorSpawnParams;
			ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;

			// spawn the projectile at the muzzle
			auto projectile = World->SpawnActor<AMonolithUniverseProjectile>(projectileClass, SpawnLocation, spawnRotation, ActorSpawnParams);
			projectile->AssignTeam(this->teamNumber);
		}
	}
}

bool AMonolithUniverseBaseCharacter::Hit_Validate(int damage)
{
	return true;
}

void AMonolithUniverseBaseCharacter::Hit_Implementation(int damage)
{
	if (this->bIsDead) return;
	if (this->currentHP > 0)
	{
		this->currentHP -= damage;

		auto controller = Cast<AMonoltihUniverseBaseController>(this->Controller);
		auto hud = Cast<UMonolithUniverseBaseHUD>(controller->GetHUDInstance());
		if (hud)
			hud->ShowColorScreen(FColor(255, 0, 0, 50), 1.0f);

		if (this->currentHP <= 0)
			Die();
	}
}

bool AMonolithUniverseBaseCharacter::Heal_Validate(int hp)
{
	return true;
}

void AMonolithUniverseBaseCharacter::Heal_Implementation(int hp)
{
	if (this->bIsDead) return;
	if (this->currentHP < this->maxHP)
	{
		this->currentHP += hp;
		if (this->currentHP > this->maxHP)
			this->currentHP = this->maxHP;
	}
}

bool AMonolithUniverseBaseCharacter::Die_Validate()
{
	return true;
}

void AMonolithUniverseBaseCharacter::Die_Implementation()
{
	OnDie();
	this->bIsDead = true;
	FTimerHandle timerHandle;
	GetWorld()->GetTimerManager().SetTimer(timerHandle, this, &AMonolithUniverseBaseCharacter::RestartPlayer, 2.5f, false);
}

void AMonolithUniverseBaseCharacter::OnDie_Implementation()
{
	if (Role < ROLE_Authority && this->Controller)
	{
//		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, "Player died.");
		auto controller = Cast<AMonoltihUniverseBaseController>(this->Controller);
		auto hud = Cast<UMonolithUniverseBaseHUD>(controller->GetHUDInstance());
		if (hud)
		{
			hud->ShowPlayerText("You died.", 2.5f);
			hud->ShowColorScreen(FColor(255,0,0,50), 2.5f);
		}
	}

}

void AMonolithUniverseBaseCharacter::RestartPlayer()
{
	auto gameMode = Cast<AMonolithUniverseBasicGameMode>(GetWorld()->GetAuthGameMode());
	gameMode->TeleporPlayerToSpawnPoint(this);
	this->currentAmmo = this->maxAmmo;
	this->currentHP = this->maxHP;
	this->bIsDead = false;
}

void AMonolithUniverseBaseCharacter::HitMe()
{
	Hit(10);
}

void AMonolithUniverseBaseCharacter::HealMe()
{
	Heal(10);
}

void AMonolithUniverseBaseCharacter::SetNickname(const FString& nickname)
{
	this->nickname = nickname;
//	GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::White, "New nickname: " + this->nickname);
	this->nicknameText->SetText(this->nickname);
}

void AMonolithUniverseBaseCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AMonolithUniverseBaseCharacter, currentAmmo);
	DOREPLIFETIME(AMonolithUniverseBaseCharacter, currentHP);
	DOREPLIFETIME(AMonolithUniverseBaseCharacter, bIsReloading);
	DOREPLIFETIME(AMonolithUniverseBaseCharacter, bIsDead);
	DOREPLIFETIME(AMonolithUniverseBaseCharacter, teamNumber);
	DOREPLIFETIME(AMonolithUniverseBaseCharacter, bIsEnhancedFireAvailable);
	DOREPLIFETIME(AMonolithUniverseBaseCharacter, bIsMovementSkillAvailable);
}

// Skills stuff
// Enhanced fire skill
void AMonolithUniverseBaseCharacter::EnhancedFireSkillCooldownStart()
{
	this->bIsEnhancedFireAvailable = false;
	GetWorld()->GetTimerManager().SetTimer(this->enhancedFireTimer, this,
		&AMonolithUniverseBaseCharacter::OnEnhancedFireSkillCooldownEnd, this->enhancedFireCooldown);
	UpdateEnhancedFireSkillOnHUD();
}
void AMonolithUniverseBaseCharacter::OnEnhancedFireSkillCooldownEnd()
{
	this->bIsEnhancedFireAvailable = true;
	GetWorld()->GetTimerManager().ClearTimer(this->enhancedFireTimer);
}
void AMonolithUniverseBaseCharacter::UpdateEnhancedFireSkillOnHUD_Implementation()
{
	auto controller = Cast<AMonoltihUniverseBaseController>(this->Controller);
	auto hud = Cast<UMonolithUniverseBaseHUD>(controller->GetHUDInstance());
	if (hud)
		hud->UpdateEnhancedFireSkill(this->enhancedFireCooldown);
}

// Movement skill
void AMonolithUniverseBaseCharacter::MovementSkillCooldownStart()
{
	this->bIsMovementSkillAvailable = false;
	GetWorld()->GetTimerManager().SetTimer(this->movementSkillTimer, this,
		&AMonolithUniverseBaseCharacter::OnMovementSkillCooldownEnd, this->movementSkillCooldown);
	UpdateMovementSkillOnHUD();
}
void AMonolithUniverseBaseCharacter::OnMovementSkillCooldownEnd()
{
	this->bIsMovementSkillAvailable = true;
	GetWorld()->GetTimerManager().ClearTimer(this->movementSkillTimer);
}
void AMonolithUniverseBaseCharacter::UpdateMovementSkillOnHUD_Implementation()
{
	auto controller = Cast<AMonoltihUniverseBaseController>(this->Controller);
	auto hud = Cast<UMonolithUniverseBaseHUD>(controller->GetHUDInstance());
	if (hud)
		hud->UpdateMovementSkill(this->movementSkillCooldown);
}

