// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameModeBase.h"
#include "SpawnPoint.h"
#include <vector>
#include "MonolithUniverseBaseCharacter.h"
#include "MonolithUniverseBaseGameState.h"
#include "MonolithUniverseBasicGameMode.generated.h"

UCLASS()
class MONOLITHUNIVERSE_API AMonolithUniverseBasicGameMode : public AGameModeBase
{
	GENERATED_BODY()

	virtual void BeginPlay() override;

	virtual void StartPlay() override;

	virtual void PostLogin(APlayerController* NewPlayer) override;

	virtual FString InitNewPlayer(APlayerController * NewPlayerController, const FUniqueNetIdRepl & UniqueId, const FString & Options, const FString & Portal) override;

public:
	/** Amount of players that is needed to start battle */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	int minPlayersAmount = 4;

private:
	int blueTeamPlayers = 0;
	int redTeamPlayers = 0;

	std::vector<ASpawnPoint*> blueSpawnPoints;
	std::vector<ASpawnPoint*> redSpawnPoints;

	int currentBlueSpawn = 0;
	int currentRedSpawn = 0;

	int battleState = 0;
	
	FTransform GetSpawnPointTransform(int teamNumber);
	int PlayersAmount() { return (this->blueTeamPlayers + this->redTeamPlayers); }
	void OnPlayerJoined();
	AMonolithUniverseBaseGameState* myGameSate;

public:
	void TeleporPlayerToSpawnPoint(AMonolithUniverseBaseCharacter* player);
};
