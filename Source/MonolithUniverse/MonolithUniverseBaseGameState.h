// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameStateBase.h"
#include "MonolithUniverseBaseGameState.generated.h"

/**
 * 
 */
UCLASS()
class MONOLITHUNIVERSE_API AMonolithUniverseBaseGameState : public AGameStateBase
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Gameplay)
	int pointsToWin = 2000;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Gameplay)
	int pointsToAdd = 10;

	/** Time between last player joined and battle start */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	float prepareBattleTime = 10.0f;
	/** Message that will be show when last players joined */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	FString prepareBattleMessage = "Prepare for the battle!";
	/** Message that will be show when battle starts*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	FString startBattleMessage = "Battle has begun!";
	/** Message that will be show when battle ends*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	FString endBattleMessage = "Battle has ended!";
	
private:
	UPROPERTY(ReplicatedUsing = OnRep_SetBlueTeamPoints)
	int blueTeamPoints;
	UPROPERTY(ReplicatedUsing = OnRep_SetRedTeamPoints)
	int redTeamPoints;

	UFUNCTION()
	void OnRep_SetBlueTeamPoints();
	UFUNCTION()
	void OnRep_SetRedTeamPoints();

	int battleState = 0;

public:
	int GetBlueTeamPoints();
	void AddBlueTeamPoints();
	void BlueTeamWin();
	int GetRedTeamPoints();
	void AddRedTeamPoints();
	void RedTeamWin();
	void Reset();

	UFUNCTION(NetMulticast, Reliable)
	void OnPrepareBattle();
	UFUNCTION(NetMulticast, Reliable)
	void OnBeginBattle();
	UFUNCTION(NetMulticast, Reliable)
	void OnEndBattle(const FString& winTeamName);
};
