// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "MonolithUniverseBaseHUD.generated.h"

/**
 * 
 */
UCLASS()
class MONOLITHUNIVERSE_API UMonolithUniverseBaseHUD : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintImplementableEvent)
	void UpdateAmmo(int current, int max);

	UFUNCTION(BlueprintImplementableEvent)
	void UpdateHP(int current, int max);

	UFUNCTION(BlueprintImplementableEvent)
	void StartReloading(float reloadTime);

	UFUNCTION(BlueprintImplementableEvent)
	void SetTeam(int teamNumber);

	UFUNCTION(BlueprintImplementableEvent)
	void UpdateRedTeamPoints(int points, int max);

	UFUNCTION(BlueprintImplementableEvent)
	void UpdateBlueTeamPoints(int points, int max);

	UFUNCTION(BlueprintImplementableEvent)
	void UpdateCapturingPoints(int points);

	UFUNCTION(BlueprintImplementableEvent)
	void UpdateCapturingState(int state);

	UFUNCTION(BlueprintImplementableEvent)
	void UpdateEnhancedFireSkill(float cooldownTime);

	UFUNCTION(BlueprintImplementableEvent)
	void UpdateMovementSkill(float cooldownTime);

	UFUNCTION(BlueprintImplementableEvent)
	void ShowPlayerText(const FString& text, float displayTime);

	UFUNCTION(BlueprintImplementableEvent)
	void ShowColorScreen(FColor color, float displayTime);
};
