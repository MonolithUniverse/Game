// Fill out your copyright notice in the Description page of Project Settings.

#include "MonolithUniverse.h"
#include "MonolithUniverseProjectile.h"
#include "MonolithUniverseBaseCharacter.h"

// Sets default values
AMonolithUniverseProjectile::AMonolithUniverseProjectile()
{
	// Set replication
	this->bReplicates = true;

	// Use a sphere as a simple collision representation
	this->collisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	this->collisionComp->InitSphereRadius(5.0f);
	this->collisionComp->BodyInstance.SetCollisionProfileName("Projectile");
	this->collisionComp->OnComponentHit.AddDynamic(this, &AMonolithUniverseProjectile::OnHit);		// set up a notification for when this component hits something blocking

																										// Players can't walk on it
	this->collisionComp->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Unwalkable, 0.f));
	this->collisionComp->CanCharacterStepUpOn = ECB_No;

	// Set as root component
	RootComponent = this->collisionComp;

	// Use a ProjectileMovementComponent to govern this projectile's movement
	this->projectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileComp"));
	this->projectileMovement->UpdatedComponent = this->collisionComp;
	this->projectileMovement->InitialSpeed = 5000.f;
	this->projectileMovement->MaxSpeed = 5000.f;
	this->projectileMovement->bRotationFollowsVelocity = true;
	this->projectileMovement->bShouldBounce = true;

	// Die after 2 seconds by default
	InitialLifeSpan = 2.0f;
}

void AMonolithUniverseProjectile::AssignTeam(int teamNumber)
{
	if (this)
	{
		this->sourceTeam = teamNumber;
	}
}

// Called when the game starts or when spawned
void AMonolithUniverseProjectile::BeginPlay()
{
	Super::BeginPlay();

}

void AMonolithUniverseProjectile::OnHit(UPrimitiveComponent* hitComp, AActor* otherActor, UPrimitiveComponent* otherComp, FVector normalImpulse, const FHitResult& hit)
{
	if (Role == ROLE_Authority) 
	{
		AMonolithUniverseBaseCharacter* player = Cast<AMonolithUniverseBaseCharacter>(otherActor);
		if (player)
		{
			if(player->GetTeamNumber() != this->sourceTeam)
				player->Hit(10);
			OnDestroy(hitComp->GetComponentLocation());
			Destroy();
		}
	}

	//// Only add impulse and destroy projectile if we hit a physics
	//if ((otherActor != NULL) && (otherActor != this) && (otherComp != NULL) && otherComp->IsSimulatingPhysics())
	//{
	//	otherComp->AddImpulseAtLocation(GetVelocity() * 100.0f, GetActorLocation());

	//	Destroy();
	//}

	if ((otherActor != NULL) && (otherActor != this) && (otherComp != NULL))
	{
		OnDestroy(hitComp->GetComponentLocation());
		Destroy();
	}

}




