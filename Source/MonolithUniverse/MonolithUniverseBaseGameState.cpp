// Fill out your copyright notice in the Description page of Project Settings.

#include "MonolithUniverse.h"
#include "MonolithUniverseBaseGameState.h"
#include "MonoltihUniverseBaseController.h"
#include "MonolithUniverseBaseHUD.h"
#include "UnrealNetwork.h"

void AMonolithUniverseBaseGameState::OnRep_SetBlueTeamPoints()
{
	auto controller = Cast<AMonoltihUniverseBaseController>(GetWorld()->GetFirstPlayerController());
	auto HUD = Cast<UMonolithUniverseBaseHUD>(controller->GetHUDInstance());

	HUD->UpdateBlueTeamPoints(this->blueTeamPoints, this->pointsToWin);
}

void AMonolithUniverseBaseGameState::OnRep_SetRedTeamPoints()
{
	auto controller = Cast<AMonoltihUniverseBaseController>(GetWorld()->GetFirstPlayerController());
	auto HUD = Cast<UMonolithUniverseBaseHUD>(controller->GetHUDInstance());

	HUD->UpdateRedTeamPoints(this->redTeamPoints, this->pointsToWin);
}

int AMonolithUniverseBaseGameState::GetBlueTeamPoints()
{
	return this->blueTeamPoints;
}

void AMonolithUniverseBaseGameState::AddBlueTeamPoints()
{
	// battle end
	if (this->battleState == 2) return;

	if (Role == ROLE_Authority)
	{
		this->blueTeamPoints += pointsToAdd;
//		GEngine->AddOnScreenDebugMessage(-1, 1, FColor::Red, "BlueTeam:" + FString::FromInt(this->blueTeamPoints));
		if (FMath::Abs(this->blueTeamPoints) >= this->pointsToWin)
			BlueTeamWin();
	}
}

void AMonolithUniverseBaseGameState::BlueTeamWin()
{
	if (Role == ROLE_Authority)
	{
//		GEngine->AddOnScreenDebugMessage(-1, 1, FColor::Red, "Blue Team Won!");

		OnEndBattle("Blue");

		FTimerHandle timerHandle;
		GetWorld()->GetTimerManager().SetTimer(timerHandle, this, &AMonolithUniverseBaseGameState::Reset, 1.f);
	}
}

int AMonolithUniverseBaseGameState::GetRedTeamPoints()
{
	return this->redTeamPoints;
}

void AMonolithUniverseBaseGameState::AddRedTeamPoints()
{
	// battle end
	if (this->battleState == 2) return;

	if (Role == ROLE_Authority)
	{
		this->redTeamPoints += pointsToAdd;
//		GEngine->AddOnScreenDebugMessage(-1, 1, FColor::Red, "RedTeam:" + FString::FromInt(this->redTeamPoints));
		if (FMath::Abs(this->redTeamPoints) >= this->pointsToWin)
			RedTeamWin();
	}
}

void AMonolithUniverseBaseGameState::RedTeamWin()
{
	if (Role == ROLE_Authority)
	{
//		GEngine->AddOnScreenDebugMessage(-1, 1, FColor::Red, "Red Team Won!");

		OnEndBattle("Red");

		FTimerHandle timerHandle;
		GetWorld()->GetTimerManager().SetTimer(timerHandle, this, &AMonolithUniverseBaseGameState::Reset, 1.f);
	}
}

void AMonolithUniverseBaseGameState::Reset()
{
	this->blueTeamPoints = 0;
	this->redTeamPoints = 0;
}

void AMonolithUniverseBaseGameState::OnPrepareBattle_Implementation()
{
	if (this->battleState == 1)
		return;

	if (Role < ROLE_Authority)
	{
		auto controller = Cast<AMonoltihUniverseBaseController>(GetWorld()->GetFirstPlayerController());
		auto hud = Cast<UMonolithUniverseBaseHUD>(controller->GetHUDInstance());
		if (hud)
			hud->ShowPlayerText(this->prepareBattleMessage, 3.0f);
	}

	FTimerHandle timerHandle;
	GetWorld()->GetTimerManager().SetTimer(timerHandle, this, &AMonolithUniverseBaseGameState::OnBeginBattle,
		this->prepareBattleTime);
}

void AMonolithUniverseBaseGameState::OnBeginBattle_Implementation()
{
	if (this->battleState == 1)
		return;

	if (Role < ROLE_Authority) 
	{
		// here should be code to unlock respawn rooms for both teams

		auto controller = Cast<AMonoltihUniverseBaseController>(GetWorld()->GetFirstPlayerController());
		auto hud = Cast<UMonolithUniverseBaseHUD>(controller->GetHUDInstance());
		if (hud)
			hud->ShowPlayerText(this->startBattleMessage, 3.0f);
	}
	
	this->battleState = 1;
}

void AMonolithUniverseBaseGameState::OnEndBattle_Implementation(const FString& winTeamName)
{
	this->battleState = 2;

	if (Role < ROLE_Authority)
	{
		auto controller = Cast<AMonoltihUniverseBaseController>(GetWorld()->GetFirstPlayerController());
		auto hud = Cast<UMonolithUniverseBaseHUD>(controller->GetHUDInstance());
		if (hud)
			hud->ShowPlayerText(winTeamName + " team won!", 3.0f);
	}
}

void AMonolithUniverseBaseGameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AMonolithUniverseBaseGameState, blueTeamPoints);
	DOREPLIFETIME(AMonolithUniverseBaseGameState, redTeamPoints);
}