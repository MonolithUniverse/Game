// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "MonolithUniverseProjectile.h"
#include "MonolithUniverseBaseCharacter.generated.h"

UCLASS()
class MONOLITHUNIVERSE_API AMonolithUniverseBaseCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Location on gun mesh where projectiles should spawn. */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USceneComponent* muzzleLocation;

	UPROPERTY(VisibleDefaultsOnly, Category = Nickname)
	class UTextRenderComponent* nicknameText;

public:
	// Sets default values for this character's properties
	AMonolithUniverseBaseCharacter();

protected:
	virtual void InitializeAttributes();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void Jump() override;

public:	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float baseTurnRate;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float baseLookUpRate;

	/** Gun muzzle's offset from the characters location */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	FVector gunOffset;

	/** Maximum amount of ammunition*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	int maxAmmo;

	/** Time that player has to wait until he reload weapon*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	float reloadTime;

	/** How much bullets player loses after primary fire*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	float primaryAtackFireCost;

	/** How much bullets player loses after enhanced fire*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	float enhancedAtackFireCost;

	/** Projectile class to spawn */
	UPROPERTY(EditDefaultsOnly, Category = Projectile)
	TSubclassOf<class AMonolithUniverseProjectile> projectileClass;

	/** Maximum health points*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	int maxHP;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	/** Accessor function for current ammo.*/
	UFUNCTION(BlueprintPure, Category = Gameplay)
	int GetCurrentAmmo() { return this->currentAmmo; }
	/** Accessor function for bIsReloading.*/
	UFUNCTION(BlueprintPure, Category = Gameplay)
	bool GetIsReloading() { return this->bIsReloading; }
	/** Accessor function for current health points.*/
	UFUNCTION(BlueprintPure, Category = Gameplay)
	int GetCurrentHP() { return this->currentHP; }
	/** Accessor function for bIsDead. */
	UFUNCTION(BlueprintPure, Category = Gameplay)
	bool GetIsDead() { return this->bIsDead; }

	// Hp management
	UFUNCTION(Server, Reliable, WithValidation)
	void Hit(int damage);
	UFUNCTION(Server, Reliable, WithValidation)
	void Heal(int hp);

	UFUNCTION(BlueprintCallable)
	int GetTeamNumber() const { return this->teamNumber; }

	UFUNCTION(BlueprintCallable)
	void AssignTeam(int teamNumber) { this->teamNumber = teamNumber; }

	void SetNickname(const FString& nickname);
protected:
	// Shooting
	UFUNCTION(Server, Reliable, WithValidation)
	void OnFire();
	virtual void OnEnhancedFire() {};
	bool CanShoot(int ammoCost);
	void LoseAmmo(int amount);
	void SpawnProjectile();
	UFUNCTION(Server, Reliable, WithValidation)
	void TryToReload();
	void Reload();
	
	UFUNCTION(Server, Reliable, WithValidation)
	void Die();
	UFUNCTION(Client, Reliable)
	void OnDie();
	void RestartPlayer();
	void HitMe(); // only for tests
	void HealMe(); // only for tests

	// Movement
	virtual void OnMovementSkillActivated() {};
	UFUNCTION()
	void MoveForward(float val);
	UFUNCTION()
	void MoveRight(float val);
	void TurnAtRate(float rate);
	void LookUpAtRate(float rate); 
	void Run();
	void Walk();
	bool bIsRunning = true;

	UPROPERTY(replicated)
	int teamNumber = -1;
	FString nickname = "";
	
	UFUNCTION(Client, Reliable)
	void DisplayTeamOnHUD();
protected:
	UPROPERTY(ReplicatedUsing = OnRep_SetCurrentAmmo)
	int currentAmmo;
	UFUNCTION()
	void OnRep_SetCurrentAmmo();

	UPROPERTY(ReplicatedUsing = OnRep_SetCurrentHP)
	int currentHP;
	UFUNCTION()
	void OnRep_SetCurrentHP();

	UPROPERTY(replicated)
	uint32 bIsDead : 1;
	UPROPERTY(replicated)
	uint32 bIsReloading : 1;

	UFUNCTION(Client, Reliable)
	void OnReload();
	FTimerHandle reloadTimer;
// Skills stuff
public:
	/** Cooldown for enhanced fire skill*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Skills)
	float enhancedFireCooldown;
	/** Cooldown for movement skill*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Skills)
	float movementSkillCooldown;
protected:
	// Enhanced fire skill 
	UPROPERTY(replicated)
	bool bIsEnhancedFireAvailable;
	FTimerHandle enhancedFireTimer;
	void EnhancedFireSkillCooldownStart();
	void OnEnhancedFireSkillCooldownEnd();
	UFUNCTION(Client, Reliable)
	void UpdateEnhancedFireSkillOnHUD();

	// Movement skill
	UPROPERTY(replicated)
	bool bIsMovementSkillAvailable;
	FTimerHandle movementSkillTimer;
	void MovementSkillCooldownStart();
	void OnMovementSkillCooldownEnd();
	UFUNCTION(Client, Reliable)
	void UpdateMovementSkillOnHUD();
	
};
