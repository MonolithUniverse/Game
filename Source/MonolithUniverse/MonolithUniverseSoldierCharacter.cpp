// Fill out your copyright notice in the Description page of Project Settings.

#include "MonolithUniverse.h"
#include "MonolithUniverseSoldierCharacter.h"
#include "MonolithUniverseBaseHUD.h"
#include "MonoltihUniverseBaseController.h"

bool AMonolithUniverseSoldierCharacter::OnEnhancedFire_Validate()
{
	return true;
}

void AMonolithUniverseSoldierCharacter::OnEnhancedFire_Implementation()
{
	if (this->bIsDead)
		return;

	if (!this->bIsEnhancedFireAvailable)
		return;

	if (CanShoot(this->enhancedAtackFireCost))
	{
		LoseAmmo(this->enhancedAtackFireCost);
		// triple shot
		SpawnProjectile();
		FTimerHandle timer1; 
		GetWorld()->GetTimerManager().SetTimer(timer1, this, &AMonolithUniverseSoldierCharacter::SpawnProjectile, 0.1f);
		FTimerHandle timer2;
		GetWorld()->GetTimerManager().SetTimer(timer2, this, &AMonolithUniverseSoldierCharacter::SpawnProjectile, 0.2f);

		EnhancedFireSkillCooldownStart();
	}
	else
	{
		TryToReload();
	}
}

bool AMonolithUniverseSoldierCharacter::OnMovementSkillActivated_Validate()
{
	return true;
}

void AMonolithUniverseSoldierCharacter::OnMovementSkillActivated_Implementation()
{
	if (this->bIsDead)
		return;

	if (!this->bIsMovementSkillAvailable)
		return;

	// Double speed
	GetCharacterMovement()->MaxWalkSpeed *= 2;
	FTimerHandle timer;
	GetWorld()->GetTimerManager().SetTimer(timer, this, &AMonolithUniverseSoldierCharacter::OnMovementSkillEnds,
		this->superRunDurationTime);

	MovementSkillCooldownStart();
}

void AMonolithUniverseSoldierCharacter::OnMovementSkillEnds()
{
	GetCharacterMovement()->MaxWalkSpeed /= 2;
}
